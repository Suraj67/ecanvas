import { Request, Response } from "express";
import { getManager, getConnection, getRepository } from "typeorm";
import { baseurl } from "../../baseurl";
import { emailSender } from "./email";
import { stringify } from "querystring";
var sizeOf = require('image-size');
const fs = require("fs");
var generator = require('generate-password');
var nodemailer = require('nodemailer');


export async function imageCategory(request: Request, response: Response) {
    const rawData = await getManager().query(`select * from get_ecanvas_image_category()`);
    response.send(rawData);
}
export async function exbCategory(request: Request, response: Response) {
 const rawData = await getManager().query(`select * from get_ecanvas_exhibition_category()`);
    response.send(rawData);
}
export async function checkUserExists(request: Request, response: Response) {
    const rawData = await getManager().query(`select * from checkuserexixts('${request.body.email}')`);
    response.send(rawData);
}
export async function checkEcanvasUserExists(request: Request, response: Response) {
    const rawData = await getManager().query(`select * from checkecanvasuserexixts('${request.body.email}','${request.body.pwd}')`);
    response.send(rawData);
}

export async function checkEmailExists(request: Request, response: Response) {
    const rawData = await getManager().query(`select * from checkemailexists('${request.body.email}')`);
    response.send(rawData);
}

export async function logoutUpdate(request: Request, response: Response) {
    const rawData = await getManager().query(`select * from logoutupdate('${request.body.logoffid}')`);
    response.send(rawData);
}

export async function registration(request: Request, response: Response) {
    let firstname: string, lastname: string, middlename: string, displayName: string,
        gender: string, address1: string, address2: string, address3: string, state: string, country: string,
        zipcode: string, email1: string, email2: string, phno1: string, phno2: string, favImg1: string, favImg2: string, userType: string, userAvatar: string,
        orgName: string, orgType: string, orgDesc: string, orgStartDate: string, orgEndDate: string, grpMail: string, loginType: string,
        userPwd: string, verifyEmailStatus: string;

    if (request.body.firstname == undefined) {
        firstname = '';
    }
    else {
        firstname = request.body.firstname;
    }
    if (request.body.middlename == undefined) {
        middlename = '';
    }
    else {
        middlename = request.body.middlename;
    }
    if (request.body.lastname == undefined) {
        lastname = '';
    }
    else {
        lastname = request.body.lastname;
    }

    if (request.body.display_name == undefined) {
        displayName = '';
    }
    else {
        displayName = request.body.display_name;
    }
    if (request.body.gender == undefined) {
        gender = '';
    }
    else {
        gender = request.body.gender;
    }
    if (request.body.address_line1 == undefined) {
        address1 = '';
    }
    else {
        address1 = request.body.address_line1;
    }
    if (request.body.address_line2 == undefined) {
        address2 = '';
    }
    else {
        address2 = request.body.address_line2;
    }
    if (request.body.address_line3 == undefined) {
        address3 = '';
    }
    else {
        address3 = request.body.address_line3;
    }
    if (request.body.state == undefined) {
        state = '';
    }
    else {
        state = request.body.state;
    }
    if (request.body.country == undefined) {
        country = '';
    }
    else {
        country = request.body.country;
    }
    if (request.body.zipcode == undefined) {
        zipcode = '';
    }
    else {
        zipcode = request.body.zipcode;
    }
    if (request.body.email_add1 == undefined) {
        email1 = '';
    }
    else {
        email1 = request.body.email_add1;
    }
    if (request.body.email_add2 == undefined) {
        email2 = '';
    }
    else {
        email2 = request.body.email_add2;
    }
    if (request.body.phone_no1 == undefined) {
        phno1 = '';
    }
    else {
        phno1 = request.body.phone_no1;
    }
    if (request.body.phone_no2 == undefined) {
        phno2 = '';
    }
    else {
        phno2 = request.body.phone_no2;
    }
    if (request.body.favorite_img_cat1 == undefined) {
        favImg1 = '';
    }
    else {
        favImg1 = request.body.favorite_img_cat1;
    }
    if (request.body.favorite_img_cat2 == undefined) {
        favImg2 = '';
    }
    else {
        favImg2 = request.body.favorite_img_cat2;
    }
    if (request.body.user_type == undefined) {
        userType = '';
    }
    else {
        userType = request.body.user_type;
    }
    if (request.body.user_avatar == undefined) {
        userAvatar = '';
    }
    else {
        userAvatar = request.body.user_avatar;
    }
    if (request.body.org_name == undefined) {
        orgName = '';
    }
    else {
        orgName = request.body.org_name;
    }
    if (request.body.org_type == undefined) {
        orgType = '';
    }
    else {
        orgType = request.body.org_type;
    }
    if (request.body.org_desc == undefined) {
        orgDesc = '';
    }
    else {
        orgDesc = request.body.org_desc;
    }
    if (request.body.org_start_date == undefined) {
        orgStartDate = null;
    }
    else {
        orgStartDate = request.body.org_start_date;
    }
    if (request.body.org_end_date == undefined) {
        orgEndDate = null;
    }
    else {
        orgEndDate = request.body.org_end_date;
    }
    if (request.body.group_email == undefined) {
        grpMail = '';
    }
    else {
        grpMail = request.body.group_email;
    }
    if (request.body.login_type == undefined) {
        loginType = '';
    }
    else {
        loginType = request.body.login_type;
    }
    if (loginType == 'ecanvas') {
        var password = generator.generate({
            length: 8,
            numbers: true,
            symbols: true
        });
        userPwd = password;
    }
    else {
        userPwd = '';
    }
    if (request.body.verify_email == undefined) {
        verifyEmailStatus = '';
    }
    else {
        verifyEmailStatus = request.body.verify_email;
    }
    let sample = `select * from registeruser('${request.body.type}',
    '${firstname}','${middlename}','${lastname}','${displayName}',
    '${gender}','${address1}','${address2}',
    '${address3}','${state}','${country}',
    '${zipcode}','${email1}','${email2}',
    '${phno1}','${phno2}','${favImg1}',
    '${favImg2}','${userType}','${userAvatar}',
    '${orgName}','${orgType}','${orgDesc}',${orgStartDate},
    ${orgEndDate},'${grpMail}','${loginType}','${userPwd}','${verifyEmailStatus}')`
    console.log(sample);

    const rawData = await getManager().query(`select * from registeruser('${request.body.type}',
    '${firstname}','${middlename}','${lastname}','${displayName}',
    '${gender}','${address1}','${address2}',
    '${address3}','${state}','${country}',
    '${zipcode}','${email1}','${email2}',
    '${phno1}','${phno2}','${favImg1}',
    '${favImg2}','${userType}','${userAvatar}',
    '${orgName}','${orgType}','${orgDesc}',${orgStartDate},
    ${orgEndDate},'${grpMail}','${loginType}','${userPwd}','${verifyEmailStatus}')`);
    if (loginType == 'ecanvas') {
        emailSender(email1, firstname, userPwd);
    }
    response.send(rawData);
}

export async function uploadArt(request: Request, response: Response) {
    let imgPath: string,
        imgThumbnail: string,
        imgDesc: string,
        category: number,
        tags: string,
        price: number,
        imgLoadChannel: string,
        imgUserId: number,
        imgSizeLength: number,
        imgSizeBreadth: number,
        imgSizeWidth: number,
        imgCanvasLength: number,
        imgCanvasBreadth: number,
        imgCanvasWidth: number,
        imgCanvasDesc: string,
        imgClassifiedAs: string,
        tagId: string,
        imgView: number,
        finaltagid: string;
    if (request.body.image == '') {
        imgPath = '';
    }
    else {
        let baseImage = request.body.image;
        const localPath = baseurl + `/src/public/uploads/${request.body.imguserid}/`;
        const ext = baseImage.substring(baseImage.indexOf("/") + 1, baseImage.indexOf(";base64"));
        const fileType = baseImage.substring("data:".length, baseImage.indexOf("/"));
        const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
        const base64Data = baseImage.replace(regex, "");
        const filename = `img-${Date.now()}.${ext}`;

        if (!fs.existsSync(localPath)) {
            fs.mkdirSync(localPath);
        }
        fs.writeFileSync(localPath + filename, base64Data, 'base64');
        imgPath = `uploads/${request.body.imguserid}/` + filename;
        var imgDimPath = localPath + filename;
        var dimensions = sizeOf(imgDimPath);
        imgSizeWidth = dimensions.width;
        imgSizeBreadth = dimensions.height;
    }

    if (request.body.imgThumbnail == undefined) {
        imgThumbnail = '';
    }
    else {
        imgThumbnail = request.body.imgThumbnail;
    }
    if (request.body.desc == undefined) {
        imgDesc = '';
    }
    else {
        const desc = request.body.desc;
        imgDesc = '';
        for (let i = 0; i < desc.length; i++) {
            imgDesc = (desc[i] == "'") ? imgDesc + desc[i] + "'" : imgDesc + desc[i];
        }
    }
    if (request.body.category == undefined) {
        category = 0;
    }
    else {
        category = request.body.category;
    }
    if (request.body.price == undefined) {
        price = 0;
    }
    else {
        price = request.body.price;
    }
    if (request.body.imgloadchannel == undefined) {
        imgLoadChannel = '';
    }
    else {
        imgLoadChannel = request.body.imgloadchannel;
    }
    if (request.body.imguserid == undefined) {
        imgUserId = 0;
    }
    else {
        imgUserId = request.body.imguserid;
    }

    if (request.body.imgThumbnail == undefined) {
        imgThumbnail = '';
    }
    else {
        imgThumbnail = request.body.imgThumbnail;
    }
    if (request.body.imgClassifiedAs == undefined) {
        imgClassifiedAs = '';
    }
    else {
        imgClassifiedAs = request.body.imgClassifiedAs;
    }
    if (request.body.tagid == undefined) {
        tagId = '';
    }
    else {
        tagId = request.body.tagid;
        console.log(tagId);
    }
    let sample = `select * from uploadart('${imgPath}','${imgThumbnail}','${imgDesc}',${category},${price},'${imgLoadChannel}',${imgUserId},0,${imgSizeBreadth},${imgSizeWidth},0,0,0,'','${imgClassifiedAs}','${tagId}',0);
    `
    console.log(sample);
    var reg = /^\d+$/;
    let testingnewtags = tagId;
    let strnewtags = testingnewtags.replace(/,/g, "");
    if (reg.test(strnewtags) == false) {
        const raw = await getManager().query(`SELECT COUNT(*) FROM ecanvas_image_tags`);
        let count = parseInt(raw[0].count);

        var res = tagId.split(",,");
        var newtags = res[1].split(",");
        for (let i = 0; i < newtags.length; i++) {
            count++;
            res[0] = res[0] + ',' + count;
            if (newtags.length - 1 != i) {
                res[0] = res[0] + ",";
            }
            const inserted = await getManager().query(`INSERT INTO ecanvas_image_tags(
                tag_name, tag_description, tag_image_exhibition)
                VALUES ('${newtags[i]}','${newtags[i]}', 0);`);
            console.log(inserted);
            console.log(newtags[i]);
        }
        console.log(res[0]);
        finaltagid = res[0].replace(/,,/g, ",");
        console.log(finaltagid);
    }
    else {
        finaltagid = tagId;
    }
    let finaltagsid = '';
    if (finaltagid[0] == ',') {
        finaltagsid = finaltagid.substr(1);
    }
    else {
        finaltagsid = finaltagid;
    }
    console.log(finaltagsid);



    const rawData = await getManager().query(`select * from uploadart('${imgPath}','${imgThumbnail}','${imgDesc}',${category},${price},'${imgLoadChannel}',${imgUserId},0,${imgSizeBreadth},${imgSizeWidth},0,0,0,'','${imgClassifiedAs}','${finaltagsid}',0);
        `);
    response.send('rawData');
}

export async function tags(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_ecanvas_image_tags()`);
    // sessions = request.session;
    // console.log(sessions.logonid);
    // console.log(request.sessions);
    // console.log(request.session.logonid);
    // console.log(request.cookies);
    // console.log(request.session.logonid);
    // request.session['sample']='hi';
    response.send(rawData);
}
export async function exbtags(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_ecanvas_exhibition_tags()`);
    response.send(rawData);
}

export async function userId(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT logon_user_id as userid FROM ecanvas_user_logon_off WHERE logonoff_id=${request.body.id};`);
    response.send(rawData);
}

export async function getImages(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_ecanvas_images(${request.body.userid});`);
    response.send(rawData);
}

export async function searchImages(request: Request, response: Response) {
   const rawData = await getManager().query(`SELECT * FROM get_ecanvas_image_list('0','${request.body.tagname}','${request.body.tagname}');`);
    response.send(rawData);
}

export async function searchExb(request: Request, response: Response) {
const rawData = await getManager().query(`SELECT * FROM get_ecanvas_exhibition_list('1','${request.body.tagname}','${request.body.tagname}');`);
    response.send(rawData);
}

export async function exbList(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT virtual_room_type_id as id,virtual_room_rent_value as amt,exb_image as image FROM ecanvas_virtual_room_type order by amt asc;`);
    response.send(rawData);
}

export async function createExbGall(request: Request, response: Response) {
    let imageids: string,
        rent_type_id: number,
        virtualExbTheme: string,
        virtualExbAvatar: string,
        user_id: number,
        virtualExbRateprdy: number,
        virtualExbTotalDay: number,
        virtualExbisactive: number,
        virtualExbprvpub: number,
        categoryid: number,
        tagId: string,
        finaltagid: string,
        virtualExbid: number,
        image_desc:string;

    imageids = (request.body.imgids == undefined) ? null : request.body.imgids;
    rent_type_id = (request.body.rentid == undefined) ? 0 : request.body.rentid;
    virtualExbTheme = (request.body.virtualExbTheme == undefined) ? '' : request.body.virtualExbTheme;
    virtualExbAvatar = (request.body.virtualExbAvatar == undefined) ? '' : request.body.virtualExbAvatar;
    user_id = (request.body.userid == undefined) ? 0 : request.body.userid;
    virtualExbRateprdy = (request.body.virtualExbRateprdy == undefined) ? 0 : request.body.virtualExbRateprdy;
    virtualExbTotalDay = (request.body.virtualExbTotalDay == undefined) ? 0 : request.body.virtualExbTotalDay;
    virtualExbisactive = (request.body.virtualExbisactive == undefined) ? 0 : request.body.virtualExbisactive;
    virtualExbprvpub = (request.body.virtualExbprvpub == undefined) ? 0 : request.body.virtualExbprvpub;
    categoryid = (request.body.categoryid == undefined) ? 0 : request.body.categoryid;
    tagId = (request.body.tagid == undefined) ? '' : request.body.tagid;
    image_desc=(request.body.desc == undefined) ? '' : request.body.desc;
    console.log(image_desc);
    // virtualExbid = (request.body.virtualExbid == undefined) ? 0:request.body.virtualExbid;

    var reg = /^\d+$/;
    let testingnewtags = tagId;
    let strnewtags = testingnewtags.replace(/,/g, "");
    if (reg.test(strnewtags) == false) {
        const raw = await getManager().query(`SELECT COUNT(*) FROM ecanvas_image_tags`);
        let count = parseInt(raw[0].count);

        var res = tagId.split(",,");
        var newtags = res[1].split(",");
        for (let i = 0; i < newtags.length; i++) {
            count++;
            res[0] = res[0] + ',' + count;
            if (newtags.length - 1 != i) {
                res[0] = res[0] + ",";
            }
            const inserted = await getManager().query(`INSERT INTO ecanvas_image_tags(
                tag_name, tag_description, tag_image_exhibition)
                VALUES ('${newtags[i]}','${newtags[i]}', 1);`);
            console.log(newtags[i]);
        }
        finaltagid = res[0].replace(/,,/g, ",");
        console.log(finaltagid);
    }
    else {
        finaltagid = tagId;
        console.log(finaltagid);
    }
    let finaltagsid = '';
    if (finaltagid[0] == ',') {
        finaltagsid = finaltagid.substr(1);
    }
    else {
        finaltagsid = finaltagid;
    }
    console.log(finaltagsid);


    const sample = `SELECT * FROM ecanvas_createexhibition_gallery('${imageids}',${rent_type_id},
    '${virtualExbTheme}','${virtualExbAvatar}',${user_id},${virtualExbRateprdy},${virtualExbTotalDay},
    ${virtualExbisactive},${virtualExbprvpub},${categoryid},'${finaltagsid}','${image_desc}');`;
    console.log(sample);

    const rawData = await getManager().query(`SELECT * FROM ecanvas_createexhibition_gallery('${imageids}',${rent_type_id},
    '${virtualExbTheme}','${virtualExbAvatar}',${user_id},${virtualExbRateprdy},${virtualExbTotalDay},
    ${virtualExbisactive},${virtualExbprvpub},${categoryid},'${finaltagsid}','${image_desc}');`);
    response.send(rawData);
}

export async function imageViewCountCalculate(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_image_view_count(${request.body.imageid});`);
    response.send(rawData);
}

export async function imageLike(request: Request, response: Response) {
    let usercomments = (request.body.comments == undefined) ? '' : request.body.comments;
    const rawData = await getManager().query(`SELECT * FROM ecanvas_image_likes(${request.body.userid},${request.body.imageid},0,0,'${usercomments}',0);`);
    response.send(rawData);
}

export async function userLikedImageid(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_image_user_like(${request.body.userid});`);
    response.send(rawData);
}

export async function categorytop4(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_ecanvas_top_category();`);
    response.send(rawData);
}

export async function tagstop6(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_ecanvas_top_tag();`);
    response.send(rawData);
}

export async function exbImages(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT ve.user_id,ve.virtualexhibition_id,ve.virtualexhibition_theme,json_agg(
        json_build_object('imageID',ii.image_id,'imageURL',ii.image_image,'positionX',gi.image_pos_x,
        'positionY',gi.image_pos_Y,'positionZ',gi.image_pos_Z,'rotationX',gi.image_rot_x,'rotationY',gi.image_rot_Y
        ,'rotationZ',gi.image_rot_Z,'rotationW',gi.image_rot_W)) AS imageDetails FROM
        ecanvas_user_info AS u INNER JOIN ecanvas_gallery_image AS gi ON gi.user_id = u.user_id
        INNER JOIN ecanvas_image_info AS ii ON gi.image_id=ii.image_id
        INNER JOIN ecanvas_virtual_exhibition AS ve ON gi.virtual_exhibition_id=ve.virtualexhibition_id
        WHERE u.user_id=${request.body.userID} AND ve.virtualexhibition_id=${request.body.exbID}
        GROUP BY ve.virtualexhibition_id,ve.virtualexhibition_theme;`);
    response.send(rawData);
}

export async function exbImagesPosition(request: Request, response: Response) {
    let asample = JSON.parse(request.body.galleryUpdate)
    let data = asample[0]
    let userID = data.user_id;
    let exbId = data.virtualexhibition_id;
    for (let i = 0; i < data.imagedetails.length; i++) {
        let imgId = data.imagedetails[i].imageID;
        let posX = data.imagedetails[i].positionX;
        let posY = data.imagedetails[i].positionY;
        let posZ = data.imagedetails[i].positionZ;
        let rotX = data.imagedetails[i].rotationX;
        let rotY = data.imagedetails[i].rotationY;
        let rotZ = data.imagedetails[i].rotationZ;
        let rotW = data.imagedetails[i].rotationW;

        // let sample1 = `
        // UPDATE ecanvas_gallery_image
        // SET image_pos_x = ${posX},
        // image_pos_y = ${posY},
        // image_pos_z = ${posZ},
        // image_rot_x = ${rotX},
        // image_rot_y = ${rotY},
        // image_rot_z = ${rotZ},
        // image_rot_w = ${rotW}
        // WHERE virtual_exhibition_id=${exbId} AND image_id=${imgId} AND user_id=${userID};`;
        // console.log(sample1);

        const rawData = await getManager().query(`
        UPDATE ecanvas_gallery_image
        SET image_pos_x = ${posX},
        image_pos_y = ${posY},
        image_pos_z = ${posZ},
        image_rot_x = ${rotX},
        image_rot_y = ${rotY},
        image_rot_z = ${rotZ},
        image_rot_w = ${rotW}
        WHERE virtual_exhibition_id=${exbId} AND image_id=${imgId} AND user_id=${userID};`);
    }
    response.send('Data Updated');
}

export async function roomrentid(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_virtual_room_type WHERE virtual_room_type_id=${request.body.roomrentid};`);
    response.send(rawData);
}

export async function exbImagesPosition2(request: Request, response: Response) {
    // console.log(request.params.galleryUpdate[0].user_id);
    // console.log(request)

    let asample = JSON.parse(request.params.galleryUpdate)
    let data = asample.galleryUpdate[0];
    console.log(data);
    let userID = data.user_id;
    let exbId = data.virtualexhibition_id;
    for (let i = 0; i < data.images.length; i++) {
        let imgId = data.images[i].imageID;
        let posX = data.images[i].positionX;
        let posY = data.images[i].positionY;
        let posZ = data.images[i].positionZ;
        let rotX = data.images[i].rotationX;
        let rotY = data.images[i].rotationY;
        let rotZ = data.images[i].rotationZ;
        let rotW = data.images[i].rotationW;

        let sample1 = `
        UPDATE ecanvas_gallery_image
        SET image_pos_x = ${posX},
        image_pos_y = ${posY},
        image_pos_z = ${posZ},
        image_rot_x = ${rotX},
        image_rot_y = ${rotY},
        image_rot_z = ${rotZ},
        image_rot_w = ${rotW}
        WHERE virtual_exhibition_id=${exbId} AND image_id=${imgId} AND user_id=${userID};`;
        console.log(sample1);

        const rawData = await getManager().query(`
        UPDATE ecanvas_gallery_image
        SET image_pos_x = ${posX},
        image_pos_y = ${posY},
        image_pos_z = ${posZ},
        image_rot_x = ${rotX},
        image_rot_y = ${rotY},
        image_rot_z = ${rotZ},
        image_rot_w = ${rotW}
        WHERE virtual_exhibition_id=${exbId} AND image_id=${imgId} AND user_id=${userID};`);
    }
    response.send('Data Updated');
}

export async function exbImages2(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT ve.user_id,ui.user_avatar,ve.virtualexhibition_id,ve.virtualexhibition_theme,ve.virtualexhibition_desc,rr.virtual_room_type_desc as virtualexhibition_theme,json_agg(
        json_build_object('imageID',ii.image_id,'imageDesc',ii.image_desc,'imageURL',ii.image_image,'positionX',gi.image_pos_x,
        'positionY',gi.image_pos_Y,'positionZ',gi.image_pos_Z,'rotationX',gi.image_rot_x,'rotationY',gi.image_rot_Y
        ,'rotationZ',gi.image_rot_Z,'rotationW',gi.image_rot_W)) AS imageDetails FROM
        ecanvas_user_info AS u 
		LEFT JOIN ecanvas_gallery_image AS gi ON gi.user_id = u.user_id
        LEFT JOIN ecanvas_image_info AS ii ON gi.image_id=ii.image_id
        LEFT JOIN ecanvas_virtual_exhibition AS ve ON gi.virtual_exhibition_id=ve.virtualexhibition_id
		LEFT JOIN ecanvas_virtual_room_type AS rr ON rr.virtual_room_type_id=ve.virtual_room_rent_type_id
        LEFT JOIN ecanvas_user_info AS ui ON ui.user_id=gi.user_id
        WHERE u.user_id=${request.params.userid} AND ve.virtualexhibition_id=${request.params.exbid}
        GROUP BY ve.virtualexhibition_id,ve.virtualexhibition_theme,ve.virtualexhibition_desc,rr.virtual_room_type_desc,ui.user_id;;`);
    response.send(rawData);
}

export async function topImages(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM get_top_images();`);
    response.send(rawData);
}

export async function allExb(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_virtual_exhibition left join ecanvas_image_category on
    ecanvas_virtual_exhibition.category_id = ecanvas_image_category.categories_id order by 
	ecanvas_virtual_exhibition.virtualexhibition_id desc limit 10`);
    response.send(rawData);
}

export async function getUserInfo(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_user_info WHERE user_id=${request.body.userid}`);
    response.send(rawData);
}



export async function updateUserInfo(request: Request, response: Response) {
    console.log(`SELECT * FROM updateuserinfo('${request.body.state}',
    '${request.body.country}',
    '${request.body.zipcode}',
    '${request.body.phno}',
    '${request.body.password}',
    '${request.body.logontype}',
    ${request.body.userid});`)
    const rawData = await getManager().query(`SELECT * FROM updateuserinfo('${request.body.state}',
    '${request.body.country}',
    '${request.body.zipcode}',
    '${request.body.phno}',
    '${request.body.password}',
    '${request.body.logontype}',
    ${request.body.userid});`);

    response.send(rawData);
}

export async function getUserExb(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_virtual_exhibition left join ecanvas_image_category on
    ecanvas_virtual_exhibition.category_id = ecanvas_image_category.categories_id WHERE ecanvas_virtual_exhibition.user_id=${request.body.userid} and ecanvas_virtual_exhibition.virtualexhibition_isdeleted=false; `);
    response.send(rawData);
}
export async function exhibitionViewCountCalculate(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_exhibition_view_count(${request.body.virtualexhibitionid});`);
    response.send(rawData);
}
export async function imageDelete(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_image_delete(${request.body.imageid});`);
    response.send(rawData);
}
export async function exhibitionDelete(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_exhibition_delete(${request.body.virtualexhibitionid});`);
    response.send(rawData);
}

export async function exbLike(request: Request, response: Response) {
    let usercomments = (request.body.comments == undefined) ? '' : request.body.comments;
    const rawData = await getManager().query(`SELECT * FROM ecanvas_exhibition_likes(${request.body.userid},${request.body.exbid},0,0,'${usercomments}',0);`);
    response.send(rawData);
}
export async function userLikedExhibitionid(request: Request, response: Response) {
    const rawData = await getManager().query(`SELECT * FROM ecanvas_exhibition_user_like(${request.body.userid});`);
    response.send(rawData);
}