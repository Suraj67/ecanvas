import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/header';
import { BrowserRouter, Switch, Route,Router,Redirect } from 'react-router-dom';
import Home from './components/container/home';
import Portfolio from './components/container/portfolio';
import Comingsoon from './components/container/Comingsoon';
import Uploadart from './components/container/uplaodart';
import Createexhibition from './components/container/createexhibition';
import Payment from './components/container/payment';
import Newexhibition from './components/container/newexhibition';
import Search from './components/container/search';
import Registration from './components/container/registration';
import NotFound from './components/container/notfound';
import'bootstrap/dist/css/bootstrap.min.css';
import './style/css/style.css';
import './style/css/font4.css';
import './style/type/icons.css';
import history from './history';
import {ProtectedRoute} from './ProtectedRoute';
import Myprofile from './components/container/MyProfile';


class App extends Component<{},any>{
  constructor(props:{}) {
    super(props);
  }
   
  render(){
  return (
    <Router history={history}>
    <Header/>
    <Switch>
        <Route exact path="/" component={Home} />
        <ProtectedRoute path="/portfolio" component={Portfolio} />
        <ProtectedRoute path="/uploadart" component={Uploadart} />
        <ProtectedRoute path="/createexhibition" component={Createexhibition} />
        <ProtectedRoute path="/payment" component={Payment} />
        <ProtectedRoute path="/newexhibition/:rentid" component={Newexhibition} />
        <Route path="/search/:data/:type" component={Search}/>
        <Route path="/reg" component={Registration} />
        <ProtectedRoute path="/myprofile" component={Myprofile} />
        <Route path="/aboutus" component={Comingsoon} />
        <Route path="*" component={NotFound}/>
    </Switch>
    </Router>
  );
}
}

export default App;
