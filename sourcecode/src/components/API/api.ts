import axios from 'axios';
import {apiurlprefix} from '../../apiconfig';
let currentuserid; 
export default {
    register:async (datas:any) => {
        const res = await axios
        .post(`${apiurlprefix}/register`, {
            type:datas.type,
            firstname:datas.firstname,
            lastname:datas.lastname,
            gender:datas.gender,
            state:datas.state,
            country:datas.country,
            zipcode:datas.zipcode,
            email_add1:datas.email_add1,
            user_avatar:datas.user_avatar,
            phone_no1:datas.phone_no1,
            org_name:datas.org_name,
            org_type:datas.org_type,
            org_desc:datas.org_desc,
            group_email:datas.group_email,
            login_type:datas.login_type,
        });
        return res.data[0].registeruser;
    },
    checkEmailExists:async(email:string)=>{
        const res = await axios.post(`${apiurlprefix}/checkemail`, {
            email: email,
          });
        return res.data[0].checkemailexists;
    },
    checkEcanvasUser:async(email:string,pwd:string)=>{
        const res = await axios.post(`${apiurlprefix}/checkecanvasuser`, {
          email: email,
          pwd: pwd,
        });
        let logonid = res.data[0].checkecanvasuserexixts.toString();
        if(logonid !== '0'){
            localStorage.setItem("logonid", logonid);
        }
        return logonid;
    },
    checkSocialUser:async(email:string)=>{
        const res = await  axios
        .post(`${apiurlprefix}/checkuser`, {
          email: email,
        })
        let logonid = res.data[0].checkuserexixts.toString();
        if(logonid !== '0'){
            localStorage.setItem("logonid", logonid);
        }
        return logonid;
    },
    CurrentUser:async()=>{
        let logoff_id = parseInt(localStorage.getItem("logonid") || "{}");
        const res = await axios
        .post(`${apiurlprefix}/currentuser`, {
          id: logoff_id,
        })
        let userid = res.data[0].userid.toString();
        localStorage.setItem("userid", userid);
        return;
    },
    Logout:async()=>{
        localStorage.removeItem("userMail");
        localStorage.removeItem("userName");
        localStorage.removeItem("userid");
        localStorage.removeItem("logontype");
        localStorage.removeItem("avatar");
        let logoffid = parseInt(localStorage.getItem("logonid") || "{}");
        const res = await axios.post(`${apiurlprefix}/logoutupdate`, {
          logoffid: logoffid,
        });
        localStorage.removeItem("logonid");
        return;
    },
    getTopTags:async () => {
        const res = await axios.get(`${apiurlprefix}/toptag`);
        return res.data;
    },
    getTopCategory:async ()=>{
        const res = await axios.get(`${apiurlprefix}/topcategory`);
        return res.data;
    },
    exblist:async()=>{
        const res = await axios.post(`${apiurlprefix}/exblist`);
        return res.data
    },
    createexb:async(datas:any)=>{
        currentuserid=datas.userid;
        const res = await axios
        .post(`${apiurlprefix}/exbgallery`, {
            imgids:datas.imgids ,
            rentid:datas.rentid ,
            virtualExbAvatar:datas.virtualExbAvatar ,
            virtualExbTheme:datas.virtualExbTheme ,
            desc:datas.desc ,
            name:datas.name ,
            userid:datas.userid ,
            virtualExbprvpub:datas.virtualExbprvpub ,
            categoryid:datas.categoryid ,
            tagid:datas.tagid ,
        });
        return res.data[0].ecanvas_createexhibition_gallery;
    },
    getexbCategory:async ()=>{
        const res = await axios.get(`${apiurlprefix}/exbcat`);
        return res.data;
    },
    getexbTags:async()=>{
        const res = await axios.get(`${apiurlprefix}/exbtags`);
        return res.data;
    },
    getexbImages:async()=>{
        let userid  = parseInt(localStorage.getItem('userid') || "{}");
        const res = await axios.post(`${apiurlprefix}/getimages`,{userid:userid});
        return res.data;
    },
    getSelectedRoom:async(roomrentid:number)=>{
        const res = await axios.post(`${apiurlprefix}/selectedroom`,{roomrentid:roomrentid});
        console.log(res.data);
        return res.data[0];
    },
    getimgCategory:async ()=>{
        const res = await axios.get(`${apiurlprefix}/imgcat`);
        return res.data;
    },
    getimgTags:async()=>{
        const res = await axios.get(`${apiurlprefix}/tags`);
        return res.data;
    },
    uploadArt:async(datas:any)=>{
        const res = await axios
        .post(`${apiurlprefix}/uploadart`, {
          image: datas.image,
          title: datas.title,
          desc: datas.desc,
          price: datas.price,
          imguserid: datas.imguserid,
          category: datas.category,
          imgClassifiedAs: datas.imgClassifiedAs,
          tagid: datas.tagid,
        });
        return res.data;
    },
    getCurrentUserid:async()=>{
        let logoff_id = parseInt(localStorage.getItem('logonid') || "{}");
        const res = await axios.post(`${apiurlprefix}/currentuser`,{id:logoff_id});
        localStorage.setItem('userid',res.data[0].userid.toString());
        return res.data[0].userid;
    },
    UserImageLike:async(id:number)=>{
        let userid = parseInt(localStorage.getItem("userid") || "{}");
        const res =  await axios.post(`${apiurlprefix}/imagelike`, {
            userid: userid,
            imageid: id,
            });
           return res.data;
    },
    getSearchImages:async(data:string)=>{
        const res =  await axios.post(`${apiurlprefix}/searchimg`, {tagname: data});
        return res.data;
    },
    getSearchExb:async(data:string)=>{
        const res = await axios.post(`${apiurlprefix}/searchexb`, {tagname: data});
        return res.data;
    },
    increaseViewCount:async(imageid:number)=>{
        const res = await axios.post(`${apiurlprefix}/imagecount`, {imageid: imageid});
        return res.data;
    },
    getUserLikedImagesId:async()=>{
        let userid  = parseInt(localStorage.getItem('userid') || "{}");
        const res = await axios.post(`${apiurlprefix}/userlikedimageid`, {userid: userid});
        return res.data;
    },
    getUserInfo:async()=>{
        let userid  = parseInt(localStorage.getItem('userid') || "{}");
        const res = await axios.post(`${apiurlprefix}/getuserinfo`,{userid: userid});
        return res.data[0];
    },
    UpdateUserInfo:async(data:any)=>{
        let userid  = parseInt(localStorage.getItem('userid') || "{}");
        const res = await axios.post(`${apiurlprefix}/updateuserinfo`,{userid:userid,state:data.state,country:data.country,zipcode:data.zipcode,phno:data.phno,password:data.password,logontype:data.logontype});
        return res;
    },
    getTopImages:async()=>{
        const res = await axios.get(`${apiurlprefix}/topimages`);
        return res.data;
    },
    getAllExb:async()=>{
        const res = await axios.get(`${apiurlprefix}/getallexb`);
        return res.data;
    },
    getUserExb:async()=>{
        let userid  = parseInt(localStorage.getItem('userid') || "{}");
        const res = await axios.post(`${apiurlprefix}/getuserexb`,{userid: userid});
        return res.data;
    },
    increaseexhibitionViewCount:async(exhibitionid:number)=>{
        const res = await axios.post(`${apiurlprefix}/exhibitioncount`, {virtualexhibitionid: exhibitionid});
        return res.data;
    },
    UserExbLike:async(id:number)=>{
        let userid = parseInt(localStorage.getItem("userid") || "{}");
        const res =  await axios.post(`${apiurlprefix}/exhibitionlike`, {
            userid: userid,
            exbid: id,
            });
           return res.data;
    },
    DeleteImg:async(imgId:number)=>{
        const res = await axios.post(`${apiurlprefix}/deleteimage`, {imageid: imgId});
        return res.data;
    },
   DeleteExb:async(exbId:number)=>{
        const res = await axios.post(`${apiurlprefix}/deleteexhibition`, {virtualexhibitionid: exbId});
        return res.data;
    }

}