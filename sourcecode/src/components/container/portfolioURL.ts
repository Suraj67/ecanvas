

export interface PhotoProps {
    alt: string;
    category?: string;
    imageUrl: string;
    caption?: string;
}

export const imgPrefix = 'assets/images/';

export const PortfolioData: PhotoProps[] = [
    {
        category: 'NATURE',
        imageUrl: `${imgPrefix}art/tg1.jpg`,
        alt: 'Lovely Couples',
        caption: 'Lovely Couples',
    },
    {
        category: 'LANDSCAPE',
        imageUrl: `${imgPrefix}art/tg2.jpg`,
        alt: 'Winter Wonderland',
        caption: 'Winter Wonderland',
    },
    {
        category: 'ART',
        imageUrl: `${imgPrefix}art/tg3.jpg`,
        alt: 'Coffee Time',
        caption: 'Coffee Time',
    },
    {
        category: 'FUN',
        imageUrl: `${imgPrefix}art/tg4.jpg`,
        alt: 'Lovely Cats',
        caption: 'Lovely Cats',
    },
    {
        category: 'URBAN',
        imageUrl: `${imgPrefix}art/tg5.jpg`,
        alt: 'Beautiful Cottages',
        caption: 'Beautiful Cottages',
    },
    {
        category: 'ART',
        imageUrl: `${imgPrefix}art/tg6.jpg`,
        alt: 'Surreal Shots',
        caption: 'Surreal Shots',
    },
    {
        category: 'LANDSCAPE',
        imageUrl: `${imgPrefix}art/tg7.jpg`,
        alt: 'Road Ahead',
        caption: 'Road Ahead',
    },
    {
        category: 'NATURE',
        imageUrl: `${imgPrefix}art/tg8.jpg`,
        alt: 'Enchanting Green',
        caption: 'Enchanting Green',
    },
    {
        category: 'FUN',
        imageUrl: `${imgPrefix}art/tg8.jpg`,
        alt: 'Charismatic Doors',
        caption: 'Charismatic Doors',
    },
];

export const ImageSectionGroups = [
    {
        title: 'Lovely Couples',
        text:
            ' Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg1-1.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-2.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-3.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-4.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-5.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-6.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-7.jpg`,
                alt: 'Lovely Couples',
            },
            {
                imageUrl: `${imgPrefix}art/tg1-8.jpg`,
                alt: 'Lovely Couples',
            },
        ],
    },
    {
        title: 'Winter Wonderland',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg2-1.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-2.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-3.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-4.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-5.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-6.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-7.jpg`,
                alt: 'Winter Wonderland',
            },
            {
                imageUrl: `${imgPrefix}art/tg2-8.jpg`,
                alt: 'Winter Wonderland',
            },
        ],
    },
    {
        title: 'Coffee Time',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg3-1.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-2.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-3.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-4.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-5.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-6.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-7.jpg`,
                alt: 'Coffee Time',
            },
            {
                imageUrl: `${imgPrefix}art/tg3-8.jpg`,
                alt: 'Coffee Time',
            },
        ],
    },
    {
        title: 'Lovely Cats',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg4-1.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-2.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-3.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-4.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-5.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-6.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-7.jpg`,
                alt: 'Lovely Cats',
            },
            {
                imageUrl: `${imgPrefix}art/tg4-8.jpg`,
                alt: 'Lovely Cats',
            },
        ],
    },
    {
        title: 'Beautiful Cottages',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg5-1.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-2.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-3.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-4.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-5.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-6.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-7.jpg`,
                alt: 'Beautiful Cottages',
            },
            {
                imageUrl: `${imgPrefix}art/tg5-8.jpg`,
                alt: 'Beautiful Cottages',
            },
        ],
    },
    {
        title: 'Surreal Shots',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg6-1.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-2.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-3.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-4.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-5.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-6.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-7.jpg`,
                alt: 'Surreal Shots',
            },
            {
                imageUrl: `${imgPrefix}art/tg6-8.jpg`,
                alt: 'Surreal Shots',
            },
        ],
    },
    {
        title: 'Road Ahead',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg7-1.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-2.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-3.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-4.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-5.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-6.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-7.jpg`,
                alt: 'Road Ahead',
            },
            {
                imageUrl: `${imgPrefix}art/tg7-8.jpg`,
                alt: 'Road Ahead',
            },
        ],
    },
    {
        title: 'Enchanting Green',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg8-1.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-2.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-3.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-4.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-5.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-6.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-7.jpg`,
                alt: 'Enchanting Green',
            },
            {
                imageUrl: `${imgPrefix}art/tg8-8.jpg`,
                alt: 'Enchanting Green',
            },
        ],
    },
    {
        title: 'Charismatic Doors',
        text:
            'Curabitur blandit tempus porttitor. Nullam quis risus egeturna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed.',
        images: [
            {
                imageUrl: `${imgPrefix}art/tg9-1.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-2.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-3.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-4.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-5.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-6.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-7.jpg`,
                alt: 'Charismatic Doors',
            },
            {
                imageUrl: `${imgPrefix}art/tg9-8.jpg`,
                alt: 'Charismatic Doors',
            },
        ],
    },
];