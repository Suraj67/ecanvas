import React from 'react'
import ReactTags from 'react-tag-autocomplete'

interface ITags{

}
class Tags extends React.Component<ITags,{tags:any,suggestions:any}> {
  constructor (props:ITags) {
    super(props)
 
    this.state = {
      tags: [
        { id: 1, name: "Wildlife" },
        { id: 2, name: "Photography" }
      ],
      suggestions: [
        { id: 3, name: "Animals" },
        { id: 4, name: "Nature" },
        { id: 5, name: "Cartoon" },
        { id: 6, name: "Art" }
      ]
    }
  }
 
  handleDelete (i: any) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }
 
  handleAddition (tag: any) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
  }
 
  render () {
    return (
        <div>
            <div className="tags">
      <ReactTags
        tags={this.state.tags}
        suggestions={this.state.suggestions}
        handleDelete={this.handleDelete.bind(this)}
        handleAddition={this.handleAddition.bind(this)}/>
        </div>
        </div>
    )
  }
}

export default Tags;