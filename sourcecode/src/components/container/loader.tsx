import React from "react";

export default function Loader() {
    return (<div className="d-flex justify-content-center">
    <img src="/assets/images/loader.gif" />
  </div>);
}