import React, { Component } from "react";
import { Flipper, Flipped } from "react-flip-toolkit";
import anime from "animejs";
import Footer2 from "../Footer/footer2";
import VirtualUrl from "../../VirtualGalleryConfig.json";
import PortfolioImage from "./portfolioimage";
import api from "../API/api";
import {apiurlprefix} from '../../apiconfig';
import Lightbox from "react-image-lightbox";
import Loader from "./loader";

interface IPortfolioState {
  catagoryList: string[];
  filter: string | undefined;
  selectView?: string;
  modalShow: boolean;
  exbimages:any;
  photoIndex: number;
  isOpen: boolean;
  loading:boolean;
}

const onElementAppear = (el: any, i: any) => {
  return anime({
    targets: el,
    opacity: 1,
    delay: i * 10,
    width: "100%",
    easing: "easeOutSine",
  });
};

const onElementExit = (el: any, i: number, onComplete: any) => {
  return anime({
    targets: el,
    opacity: 0,
    delay: i * 10,
    easing: "easeOutSine",
    complete: onComplete,
  });
};

class Portfolio extends Component<{}, IPortfolioState> {
  constructor(props: {}) {
    super(props);

    // const cat: string[] = PortfolioData.map((x) => x.category).filter(
    //   (curr, i, arr) => arr.indexOf(curr) === i
    // ) as string[];

    this.state = {
      catagoryList: [],
      filter: undefined,
      selectView: "",
      modalShow: false,
      exbimages:[],
      photoIndex: 0,
      isOpen: false,
      loading:true
    };
  }

  componentDidMount() {
    this.onload();
  }
  async onload() {
    let res = await api.getexbImages();
    console.log(res);
    const cat: string[] = res.map((x:any) => x.category).filter(
        (curr:any, i:any, arr:any) => arr.indexOf(curr) === i) as string[];
    this.setState({catagoryList:cat,exbimages:res,loading:false})
  }
  PopupIndex(id: number) {
    let selectedIndex = 0;
    for (let i = 0; i < this.state.exbimages.length; i++) {
      if (id == this.state.exbimages[i].id) {
        selectedIndex = i;
      }
    }
    this.setState({ isOpen: true, photoIndex: selectedIndex });
  }
  async deleteImg(id:number){
    if (window.confirm("Aru you sure you want to delete this image?")) {
      let res = await api.DeleteImg(id);
      this.onload();
    } else {
      
    }

   

  }
  render() {
    const { catagoryList } = this.state;

    const selectCategory = (element: string) => {
      this.setState({
        filter: element,
      });
    };
    
    let isOpen = this.state.isOpen;
    let photoIndex = this.state.photoIndex;
    const images: string | any[] = [];

    for (let i = 0; i < this.state.exbimages.length; i++) {
      let imageurl =
        `${apiurlprefix}/` + this.state.exbimages[i].image;
      images.push(imageurl);
    }

    

    return (
      <div>
        <div
          className="wrapper image-wrapper bg-image inverse-text latest_container"
          style={{ backgroundImage: "url(assets/images/art/bg12.jpg)" }}
        >
          <div className="container inner pt-120 pb-120 text-center montfont">
            <h1 className="heading montfont">My Recent Images</h1>
            <p className="lead larger text-center mb-0 montfont">
              Minimalism meets photography
            </p>
          </div>
    </div>{this.state.loading?<div className="loader_margin">
      <Loader/>
    </div>:(this.state.exbimages.length!==0)? <Flipper flipKey={this.state.filter}>
          <div className="wrapper light-wrapper">
            <div className="space30"></div>
            <div
              id="cube-grid-full-filter"
              className="cbp-filter-container text-center"
            >
              <div
                onClick={() => selectCategory("")}
                className={`${
                  !this.state.filter ? "cbp-filter-item-active montfont" : ""
                } cbp-filter-item montfont`}
              >
                All
              </div>
              {catagoryList.map((item) => (
                <FilterPanel
                  catName={item}
                  key={item}
                  classes={`${
                    this.state.filter == item
                      ? "cbp-filter-item-active montfont"
                      : ""
                  } cbp-filter-item montfont`}
                  onSelect={selectCategory}
                />
              ))}
            </div>
            <div className="clearfix"></div>
            <div className="space20"></div>
            <div id="cube-grid-full" className='card-columns'>
              {this.state.exbimages.sort()
                .filter((d:any) => {
                  if (!this.state.filter) {
                    return true;
                  }
                  return d.category === this.state.filter;
                })
                .map((item:any, i:any) => {
                  return (
                    <Flipped
                      //flipId={`item-${item.category}=${i}`}
                      flipId={`item-${item.image}`}
                      onAppear={onElementAppear}
                      onStart={onElementAppear}
                      onExit={onElementExit}
                      key={`item-${item.category}=${i}`}
                    >
                      <div className="card">
                      <i className="fa fa-trash delicon" aria-hidden="true" onClick={() => {
                  this.deleteImg(item.id);}}></i>
                        <figure
                          onClick={() => this.PopupIndex(item.id)}
                          className="overlay-info hovered overlay-portfolio"
                        >
                         
                          <img src={`${apiurlprefix}/`+item.image} />
                          <figcaption className="d-flex">
                            <div className="align-self-center mx-auto">
                              <h3 className="mb-0 montfont">{item.artist}</h3>
                            </div>
                          </figcaption>
                        </figure>
                        {/* <button  className="btn btn-full-rounded" onClick={() => {
                  this.deleteImg(item.id);
                }}>delete</button> */}
                      </div>
                    </Flipped>
                  );
                })}
            </div>
          </div>
        </Flipper>: <h2 className="section-title text-center montfont">
             You Have No Images
            </h2>}
        
        <div>
              {isOpen && (
                <Lightbox
                  mainSrc={images[photoIndex]}
                  nextSrc={images[(photoIndex + 1) % images.length]}
                  prevSrc={
                    images[(photoIndex + images.length - 1) % images.length]
                  }
                  onCloseRequest={() => this.setState({ isOpen: false })}
                  onMovePrevRequest={() => {
                    this.setState({
                      photoIndex:
                        (photoIndex + images.length - 1) % images.length,
                    });
                  }}
                  onMoveNextRequest={() => {
                    this.setState({
                      photoIndex: (photoIndex + 1) % images.length,
                    });
                  }}
                />
              )}
            </div>
        <div className="space40"></div>
        <PortfolioImage />
        <Footer2 />
      </div>
    );
  }
}

interface IFilterPanelProps {
  onSelect: (cat: string) => void;
  catName: string;
  classes: string;
}
const FilterPanel = (props: IFilterPanelProps) => {
  return (
    <div
      className={props.classes}
      onClick={() => props.onSelect(props.catName)}
    >
      {props.catName}
    </div>
  );
};

export default Portfolio;
