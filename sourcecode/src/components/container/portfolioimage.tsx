import React, { Component } from "react";
import { Flipper, Flipped } from "react-flip-toolkit";
import anime from "animejs";
import { PortfolioData, ImageSectionGroups, PhotoProps } from "./portfolioURL";
import Footer2 from "../Footer/footer2";
import VirtualUrl from "../../VirtualGalleryConfig.json";
import api from '../API/api';
import {apiurlprefix} from '../../apiconfig';
import Loader from "./loader";
import {isMobile} from "react-device-detect";

interface IPortfolioState {
  catagoryList: string[];
  filter: string | undefined;
  selectView?: string;
  exblist:any;
  iframeurl:string;
  loading:boolean;
  windowWidth:any;
}

interface IImageSections {
  title: string;
  text: string;
  images: PhotoProps[];
}

const onElementAppear = (el: any, i: any) => {
  return anime({
    targets: el,
    opacity: 1,
    delay: i * 10,
    width: "100%",
    easing: "easeOutSine",
  });
};

const onElementExit = (el: any, i: number, onComplete: any) => {
  return anime({
    targets: el,
    opacity: 0,
    delay: i * 10,
    easing: "easeOutSine",
    complete: onComplete,
  });
};

class PortfolioImage extends Component<{}, IPortfolioState> {
  constructor(props: {}) {
    super(props);

    const cat: string[] = PortfolioData.map((x) => x.category).filter(
      (curr, i, arr) => arr.indexOf(curr) === i
    ) as string[];

    this.state = {
      catagoryList: [],
      filter: undefined,
      selectView: "",
      exblist:[],
      iframeurl:'',
      loading:true,
      windowWidth: window.innerWidth
    };
  }
  handleResize = (e:Event) => {
    this.setState({ windowWidth: window.innerWidth });
   };
  componentDidMount(){
    this.onload();
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
   } 
   InfoMsg(){
    alert("For the best experience please use a laptop or fesktop computer. The interactive viewer is not supported on mobile devices yet, but stay tuned for the mobile app.");
  }
  async onload(){
    let res = await api.getUserExb();
    console.log(res);
    const cat: string[] = res.map((x:any) => x.categories_name).filter(
      (curr:any, i:any, arr:any) => arr.indexOf(curr) === i
    ) as string[];
      this.setState({catagoryList:cat,exblist:res,loading:false})

    console.log(res); 
  }
  iframeload(userid :number,exbid:number){
    let logonUserId = 0;
    if(localStorage.getItem('userid') !== null){
      logonUserId = parseInt(localStorage.getItem('userid') || '');
    }
    this.setState({iframeurl:`${apiurlprefix}/`+
    VirtualUrl.virtualGalleryUrl +
    `?userid=${userid}&exhibitionid=${exbid}&loggeduserid=${logonUserId}`})
  }
  async deleteExb(id:number){
   
    

    if (window.confirm("Aru you sure you want to delete this exhibition?")) {
      let res = await api.DeleteExb(id);
      this.onload();
    } else {
      
    }
  }
  render() {
    const { catagoryList } = this.state;
    const { windowWidth } = this.state; 

    const selectCategory = (element: string) => {
      this.setState({
        filter: element,
      });
    };

    return (
      <div>
        <div
          className="wrapper image-wrapper bg-image inverse-text latest_container"
          style={{ backgroundImage: "url(assets/images/art_bak/bg12.jpg)" }}
        >
        </div>
        {this.state.loading?<div className="loader_margin">
      <Loader/>
    </div>:(this.state.exblist.length!==0)?<Flipper flipKey={this.state.filter}>
          <div className="wrapper light-wrapper">
            <div className="space30"></div>
            <div
              id="cube-grid-full-filter"
              className="cbp-filter-container text-center"
            >
              <div
                onClick={() => selectCategory("")}
                className={`${
                  !this.state.filter ? "cbp-filter-item-active montfont" : ""
                } cbp-filter-item montfont`}
              >
                All
              </div>
              {catagoryList.map((item) => (
                <FilterPanel
                  catName={item}
                  key={item}
                  classes={`${
                    this.state.filter == item ? "cbp-filter-item-active montfont" : ""
                  } cbp-filter-item montfont`}
                  onSelect={selectCategory}
                />
              ))}
            </div>
            <div className="clearfix"></div>
            <div className="space20"></div>
            <div id="cube-grid-full" className="card-columns">
              {this.state.exblist.sort()
                .filter((d:any) => {
                  if (!this.state.filter) {
                    return true;
                  }
                  return d.categories_name === this.state.filter;
                })
                .map((item:any, i:any) => {
                  return (
                    <Flipped
                      //flipId={`item-${item.category}=${i}`}
                      flipId={`item-${item.virtualexhibition_avatar}`}
                      onAppear={onElementAppear}
                      onStart={onElementAppear}
                      onExit={onElementExit}
                      key={`item-${item.categories_name}=${i}`}
                    >
                      <div className="card">
                      <i className="fa fa-trash delicon" aria-hidden="true" onClick={() => {
                  this.deleteExb(item.virtualexhibition_id);}}></i>
                {(!isMobile)
                              ?
                        <button
                          data-toggle="modal"
                          data-target="#myModal"
                          data-backdrop="static" data-keyboard="false"
                          className="button_slider prelative w-100"
                          onClick={()=>this.iframeload(item.user_id,item.virtualexhibition_id)}
                        >
                         
                          <figure className="overlay-info hovered overlay-portfolio">
                            <img src={`${apiurlprefix}/`+item.virtualexhibition_avatar} />
                            <figcaption className="d-flex">
                              <div className="align-self-center mx-auto">
                                <h3 className="mb-0 montfont">{item.virtualexhibition_theme}</h3>
                              </div>
                            </figcaption>
                          </figure>
                        </button>
                        :
                        <button
                         
                          className="button_slider prelative w-100"
                          onClick={()=>this.InfoMsg()}
                        >
                         
                          <figure className="overlay-info hovered overlay-portfolio">
                            <img src={`${apiurlprefix}/`+item.virtualexhibition_avatar} />
                            <figcaption className="d-flex">
                              <div className="align-self-center mx-auto">
                                <h3 className="mb-0 montfont">{item.virtualexhibition_theme}</h3>
                              </div>
                            </figcaption>
                          </figure>
                        </button>
                }
                 {/* <button className="btn btn-full-rounded" onClick={() => {
                  this.deleteExb(item.virtualexhibition_id);
                }}>delete</button> */}
                      </div>
                    </Flipped>
                  );
                })}
            </div>
          </div>
        </Flipper>:<h2 className="section-title text-center montfont">
             You Not Yet Created Exhibitions
            </h2>}
        
        <div className="modal" id="myModal" tabIndex={-1}>
          <div className="modal-dialog">
            <div className="modal-content">
              <iframe
                src={this.state.iframeurl}
                id="popup1"
                title="popup"
                className="modal-body"
                style={{ width: "100%", minHeight: "550px" }}
              ></iframe>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-full-rounded"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

interface IFilterPanelProps {
  onSelect: (cat: string) => void;
  catName: string;
  classes: string;
}
const FilterPanel = (props: IFilterPanelProps) => {
  return (
    <div
      className={props.classes}
      onClick={() => props.onSelect(props.catName)}
    >
      {props.catName}
    </div>
  );
};

export default PortfolioImage;
