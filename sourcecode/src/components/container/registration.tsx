import React, { Component } from "react";
import DatePicker from "react-date-picker";
import Footer2 from "../Footer/footer2";
import axios from "axios";
import { Globaldata } from "./sample";
import history from "../../history";
import { apiurlprefix } from "../../apiconfig";
import Popup from "reactjs-popup";
import api from '../API/api';

interface IRegistration {
  selectValue: string;
  individual: string;
  nonIndividual: string;
  date: any;
  userMail: string;
  userName: string;
  type: string;
  lastName: string;
  gender1: string;
  state1: string;
  country1: string;
  zipcode1: string;
  phNo1: string;
  subscription: string;
  orgName: string;
  orgType: string;
  orgDesc: string;
  grpUser: string;
  formValid: boolean;
  borderClr: string;
  loginType: string;
  emailExists: string;
  emailExistsBorder: string;
  isOpen: boolean;
  emailEmpty:string;
  btnLoader:boolean;
}
class Registration extends Component<{}, IRegistration> {
  constructor(props: IRegistration) {
    super(props);
    this.state = {
      selectValue: "yes",
      individual: "block",
      nonIndividual: "none",
      date: null,
      userMail: "",
      userName: "",
      type: "user",
      lastName: "",
      gender1: "Male",
      state1: "",
      country1: "",
      zipcode1: "",
      phNo1: "",
      subscription: "",
      orgName: "",
      orgType: "",
      orgDesc: "",
      grpUser: "",
      formValid: false,
      borderClr: "",
      loginType: "",
      emailExists: "none",
      emailExistsBorder: "",
      isOpen: false,
      emailEmpty:'none',
      btnLoader:false,
    };
    this.handleChange = this.handleChange.bind(this);
  }
  onChange = (date: any) => this.setState({ date });

  handleChange(e: any) {
    this.setState({ selectValue: e.target.value });
    if (this.state.selectValue == "yes") {
      this.setState({
        individual: "none",
        nonIndividual: "block",
        type: "org",
      });
    } else {
      this.setState({
        individual: "block",
        nonIndividual: "none",
        type: "user",
      });
    }
  }

  lastName(e: any) {
    this.setState({ lastName: e.target.value });
  }
  gender1(e: any) {
    this.setState({ gender1: e.target.value });
  }
  state1(e: any) {
    this.setState({ state1: e.target.value });
  }
  country1(e: any) {
    this.setState({ country1: e.target.value });
  }
  zipcode1(e: any) {
    this.setState({ zipcode1: e.target.value });
  }
/*  phNo1(e: any) {
    this.setState({ phNo1: e.target.value });
  }*/

  phNo1(e: any) {
    
    let phstr = /^[\d ()+-]+$/.test(e.target.value.toString());
    console.log(phstr);
    phstr?this.setState({ phNo1: e.target.value }): this.setState({ phNo1: e.target.value.slice(0,-1) });;
  }
  subscription(e: any) {
    this.setState({ subscription: e.target.value });
  }
  orgName(e: any) {
    this.setState({ orgName: e.target.value });
    this.setState({ borderClr: "1px solid rgba(21, 21, 21, 0.15)" });
    if (e.target.value != "") {
      this.setState({
        formValid: true,
      });
    } else {
      this.setState({ formValid: false, borderClr: "1px solid red" });
    }
  }
  orgType(e: any) {
    this.setState({ orgType: e.target.value });
  }
  orgDesc(e: any) {
    this.setState({ orgDesc: e.target.value });
  }
  grpUser(e: any) {
    this.setState({ grpUser: e.target.value });
  }
  userName(e: any) {
    this.setState({ userName: e.target.value });
  }
  userMail(e: any) {
    this.setState({ userMail: e.target.value });
    this.setState({ emailEmpty: "none",emailExists:'none', emailExistsBorder: "" });
    if (e.target.value != "") {
      this.setState({
        formValid: true,
      });
    } else {
      this.setState({ formValid: false,emailEmpty: "block", emailExistsBorder: "1px solid red" });
    }
  }

  async register() {
    let avatar = "";
    if (this.state.loginType == "google") {
      avatar = localStorage.getItem("avatar") || "";
    }

    const datas = {
      type: this.state.type,
        firstname: this.state.userName,
        lastname: this.state.lastName,
        gender: this.state.gender1,
        state: this.state.state1,
        country: this.state.country1,
        zipcode: this.state.zipcode1,
        email_add1: this.state.userMail,
        user_avatar: avatar,
        phone_no1: this.state.phNo1,
        subscription: this.state.subscription,
        org_name: this.state.userName,
        org_type: this.state.orgType,
        org_desc: this.state.orgDesc,
        group_email: this.state.grpUser,
        login_type: this.state.loginType,
    }
    let logonid = await api.register(datas);
      this.setState({btnLoader:false});
      $(".register_btn").click();
      
  }

  redirectHome() {
    history.push("/");
  }

  async emailCheck(email:string){
    let checkStat = await api.register(email);
  }

  async handleSubmitUser(e: any) {
    e.preventDefault();
    this.setState({btnLoader:true})
    let emailexists;
    if(this.state.userMail == ''){
      this.setState({
        emailEmpty: "block",
        emailExistsBorder: "1px solid red",
        btnLoader:false
      });
    }
    else{
      emailexists = await api.checkEmailExists(this.state.userMail)
        if (emailexists == 0) {
          this.register();
        } else {
          this.setState({
            emailExists: "block",
            emailExistsBorder: "1px solid red",
            btnLoader:false
          });
        }
    }
   
    
  }

  async handleSubmitOrg(e: any) {
    e.preventDefault();
    this.setState({btnLoader:true})
    let emailexists;
    if(this.state.userMail == '' || this.state.orgName==''){
      if(this.state.userMail == ''){
        this.setState({
          emailEmpty: "block",
          emailExistsBorder: "1px solid red",
          btnLoader:false
        });
      }
      if(this.state.orgName == ''){
        this.setState({ borderClr: "1px solid red",btnLoader:false });
      }
    }
    else{
      let emailstatus = true;
      emailexists = await api.checkEmailExists(this.state.userMail);
        if (emailexists == 0) {
          emailstatus=true;
        } else {
          emailstatus=false;
        }
        if (emailstatus) {
          console.log(emailexists)
          this.register();
        } else {
            this.setState({
              emailExists: "block",
              emailExistsBorder: "1px solid red",
              btnLoader:false
            });
        }
    }
  }
  componentDidMount() {
    const profileMail = localStorage.getItem("userMail") || "{}";
    const profileName = localStorage.getItem("userName") || "{}";
    const loginType = localStorage.getItem("logontype");

    if (loginType != null) {
      this.setState({
        userMail: profileMail,
        userName: profileName,
        loginType: loginType,
      });
    } else {
      this.setState({ loginType: "ecanvas" });
    }
  }
  componentWillUnmount(){
    localStorage.removeItem("userMail");
    localStorage.removeItem("userName");
    localStorage.removeItem("logontype");
    localStorage.removeItem("avatar");
  }

  render() {
    let indForm = this.state.individual;
    let nonindForm = this.state.nonIndividual;
    let selValue = this.state.selectValue;
    Globaldata.logintype = localStorage.getItem('logontype') || '';
    const signupRenderFirstName = () => {
      if (
        Globaldata.logintype == "facebook" ||
        Globaldata.logintype == "google"
      ) {
        return (
          <div className="col-sm-9">
            <input
              type="text"
              className="form-control"
              id="fname"
              defaultValue={this.state.userName}
              placeholder="First Name"
              readOnly={true}
            />
          </div>
        );
      } else {
        return (
          <div className="col-sm-9">
            <input
              type="text"
              className="form-control"
              id="fname"
              value={this.state.userName}
              placeholder="First Name"
              onChange={(e) => this.userName(e)}
            />
          </div>
        );
      }
    };
    const signupRenderMail = () => {
      if (
        Globaldata.logintype == "google" ||
        Globaldata.logintype == "facebook"
      ) {
        return (
          <div className="col-sm-9">
            <input
              type="email"
              className="form-control"
              id="inputEmail2"
              defaultValue={this.state.userMail}
              placeholder="Email"
              readOnly={true}
            />
          </div>
        );
      } else {
        return (
          <>
            <div className="col-sm-9">
              <input
                type="email"
                className="form-control"
                id="inputEmail2"
                value={this.state.userMail}
                placeholder="Email"
                onChange={(e) => this.userMail(e)}
                style={{ border: this.state.emailExistsBorder }}
              />
            </div>
            <div
              className="emailexists col-sm-12"
              style={{ display: this.state.emailExists }}
            >
              *Email Already Exists
            </div>
            <div
              className="emailempty col-sm-12"
              style={{ display: this.state.emailEmpty }}
            >
              *Email Cannot be Empty
            </div>
          </>
        );
      }
    };
    return (
      <div>
        {/* {this.redirectHome()} */}
        <div className="wrapper light-wrapper registration">
          <div className="container inner pt-60">
            <div className="row mulifont">
              <div className="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                <h2 className="section-title mb-40 text-center">
                  Registration
                </h2>
                <div className="form-group row">
                  <label
                    htmlFor="individual"
                    className="col-sm-3 col-form-label"
                  >
                    I am an Individual
                  </label>
                  <div className="custom-select-wrapper col-sm-9">
                    <select
                      className="custom-select"
                      name="individual"
                      value={selValue}
                      onChange={this.handleChange}
                    >
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                </div>
                <form
                  style={{ display: indForm }}
                  onSubmit={(e) => this.handleSubmitUser(e)}
                >
                  <div className="form-group row">
                    <label htmlFor="fname" className="col-sm-3 col-form-label">
                      First Name
                    </label>
                    {signupRenderFirstName()}
                  </div>
                  <div className="form-group row">
                    <label htmlFor="lname" className="col-sm-3 col-form-label">
                      Last Name
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="lname"
                        name="lastName"
                        value={this.state.lastName}
                        placeholder="Last Name"
                        onChange={(e) => this.lastName(e)}
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="inputEmail2"
                      className="col-sm-3 col-form-label"
                    >
                      Email
                    </label>
                    {signupRenderMail()}
                  </div>
                  <div className="form-group row">
                    <label htmlFor="dob" className="col-sm-3 col-form-label">
                      DOB
                    </label>
                    <div className="col-sm-9">
                      <DatePicker
                        onChange={this.onChange}
                        value={this.state.date}
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="gender" className="col-sm-3 col-form-label">
                      Gender
                    </label>
                    <div className="col-sm-9">
                      <div className="select-type custom-select-wrapper">
                        <select
                          name="gender1"
                          className="custom-select"
                          value={this.state.gender1}
                          onChange={(e) => this.gender1(e)}
                        >
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                          <option value="others">Others</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="state" className="col-sm-3 col-form-label">
                      State
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="state"
                        name="state1"
                        value={this.state.state1}
                        onChange={(e) => this.state1(e)}
                        placeholder="State"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="country"
                      className="col-sm-3 col-form-label"
                    >
                      Country
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="country"
                        name="country1"
                        value={this.state.country1}
                        onChange={(e) => this.country1(e)}
                        placeholder="Country"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="zipcode"
                      className="col-sm-3 col-form-label"
                    >
                      Zipcode
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="zipcode"
                        name="zipcode1"
                        value={this.state.zipcode1}
                        onChange={(e) => this.zipcode1(e)}
                        placeholder="Zipcode"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="phnumber"
                      className="col-sm-3 col-form-label"
                    >
                      Phone Number
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="tel"
                        className="form-control"
                        maxLength={15}
                        id="phnumber"
                        name="phNo1"
                        value={this.state.phNo1}
                        onChange={(e) => this.phNo1(e)}
                        placeholder=""
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="package" className="col-sm-3 col-form-label">
                      subscription
                    </label>
                    <div className="col-sm-9">
                      <div className="select-type custom-select-wrapper">
                        <select
                         name="subscription"
                         className="custom-select"
                         value={this.state.subscription}
                         onChange={(e) => this.subscription(e)}
                         >
                           <option value="basic">Basic</option>
                           <option value="standard">Standard</option>
                           <option value="premium">Premium</option>
                         </select>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-sm-10">
                      <button type="submit" className="btn btn-full-rounded" disabled={this.state.btnLoader}>
                      {this.state.btnLoader && (<i
              className="fa fa-refresh fa-spin"
              style={{ marginRight: "5px" }}
            />)}Register
                      </button>
                    </div>
                  </div>
                </form>
                <form
                  style={{ display: nonindForm }}
                  onSubmit={(e) => this.handleSubmitOrg(e)}
                >
                  <div className="form-group row">
                    <label
                      htmlFor="orgname"
                      className="col-sm-3 col-form-label"
                    >
                      Organization Name
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="orgname"
                        value={this.state.orgName}
                        onChange={(e) => this.orgName(e)}
                        style={{ border: this.state.borderClr }}
                        placeholder="Organization Name"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="orgtype"
                      className="col-sm-3 col-form-label"
                    >
                      Organization Type
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="orgtype"
                        name="orgType"
                        value={this.state.orgType}
                        onChange={(e) => this.orgType(e)}
                        placeholder="Organization Type"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="orgdesc"
                      className="col-sm-3 col-form-label"
                    >
                      Organization Description
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="orgdesc"
                        name="orgDesc"
                        value={this.state.orgDesc}
                        onChange={(e) => this.orgDesc(e)}
                        placeholder="Organization Description"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="inputEmail"
                      className="col-sm-3 col-form-label"
                    >
                      Email
                    </label>
                    {signupRenderMail()}
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="grpUser"
                      className="col-sm-3 col-form-label"
                    >
                      Group Users
                    </label>
                    <div className="col-sm-9">
                      <textarea
                        name="grpUser"
                        id=""
                        cols={30}
                        rows={10}
                        value={this.state.grpUser}
                        onChange={(e) => this.grpUser(e)}
                        placeholder="Provide multiple email separated by ;"
                      ></textarea>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="gender2"
                      className="col-sm-3 col-form-label"
                    >
                      Gender
                    </label>
                    <div className="col-sm-9">
                      <div className="select-type custom-select-wrapper">
                        <select
                          name="gender2"
                          className="custom-select"
                          value={this.state.gender1}
                          onChange={(e) => this.gender1(e)}
                        >
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                          <option value="others">Others</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="state2" className="col-sm-3 col-form-label">
                      State
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="state2"
                        name="state2"
                        value={this.state.state1}
                        onChange={(e) => this.state1(e)}
                        placeholder="State"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="country2"
                      className="col-sm-3 col-form-label"
                    >
                      Country
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="country2"
                        name="country2"
                        value={this.state.country1}
                        onChange={(e) => this.country1(e)}
                        placeholder="Country"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="zipcode2"
                      className="col-sm-3 col-form-label"
                    >
                      Zipcode
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="zipcode2"
                        name="zipcode2"
                        value={this.state.zipcode1}
                        onChange={(e) => this.zipcode1(e)}
                        placeholder="Zipcode"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="phnumber2"
                      className="col-sm-3 col-form-label"
                    >
                      Phone Number
                    </label>
                    <div className="col-sm-9">
                      <input type="tel" 
                        maxLength={15}
                        className="form-control"
                        id="phnumber2"
                        name="phNo2"
                        value={this.state.phNo1}
                        onChange={(e) => this.phNo1(e)}
                        placeholder=""
                       
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="package2"
                      className="col-sm-3 col-form-label"
                    >
                      subscription
                    </label>
                    <div className="col-sm-9">
                      <div className="select-type custom-select-wrapper">
                        <select
                          name="subscription"
                          className="custom-select"
                          value={this.state.subscription}
                          onChange={(e) => this.subscription(e)}
                        >
                          <option value="basic">Basic</option>
                          <option value="standard">Standard</option>
                          <option value="premium">Premium</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-sm-10">
                    <button type="submit" className="btn btn-full-rounded" disabled={this.state.btnLoader}>
                      {this.state.btnLoader && (<i
              className="fa fa-refresh fa-spin"
              style={{ marginRight: "5px" }}
            />)}Register
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <Popup trigger={<button className="register_btn"></button>} modal>
            <div>
              <div className="col-lg-8 col-sm-8 col-12 main-section">
                <div className="modal-content">
                  <div className="col-lg-12 col-sm-12 col-12 user-name text-center">
                    <h5>Registration completed successfully.</h5>
                    <div className="user-name text-center">
                      Please check your registered email for login details.
                    </div>
                  </div>
                  <div className="col-lg-12 col-sm-12 col-12 form-input text-center">
                    <button
                      className="btn btn-full-rounded"
                      onClick={this.redirectHome}
                    >
                      OK
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Popup>
        </div>
        <Footer2 />
      </div>
    );
  }
}

export default Registration;
