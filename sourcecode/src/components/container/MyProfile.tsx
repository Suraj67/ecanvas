import React from "react";
import history from "../../history";
import api from "../API/api";
import Loader from './loader';

interface IMyprofile {
  firstname: string;
  lastName: string;
  gender: string;
  state: string;
  country: string;
  zipcode: number;
  phno: number;
  email: string;
  statevalid: string;
  countryvalid: string;
  zipcodevalid: string;
  phnovalid: string;
  pwdChecked: boolean;
  loading: boolean;
  logonType: string;
  currentPassword: string;
  currentPwdCheck: string;
  newPwd: string;
  confirmPwd: string;
  checkCurrPwd: string;
  newPwdValid: string;
  confirmPwdValid: string;
  passwordCheck: string;
  newPwdMatch: string;
  pwdCheckBorder: string;
  pwdMatchBorder: string;
}

export default class Myprofile extends React.Component<{}, IMyprofile> {
  constructor(props: {}) {
    super(props);
    this.state = {
      firstname: "",
      lastName: "",
      gender: "",
      state: "",
      country: "",
      zipcode: 123456,
      phno: 8888888888,
      email: "",
      statevalid: "",
      countryvalid: "",
      zipcodevalid: "",
      phnovalid: "",
      pwdChecked: false,
      loading: true,
      logonType: "",
      currentPassword: "",
      currentPwdCheck: "",
      newPwd: "",
      confirmPwd: "",
      checkCurrPwd: "",
      newPwdValid: "",
      confirmPwdValid: "",
      passwordCheck: "none",
      newPwdMatch: "none",
      pwdCheckBorder: "",
      pwdMatchBorder: "",
    };
  }
  // lastName(e: any) {
  //   this.setState({ lastName: e.target.value });
  // }
  // gender1(e: any) {
  //   this.setState({ gender: e.target.value });
  // }
  state1(e: any) {
    this.setState({ state: e.target.value });
    e.target.value === ""
      ? this.setState({ statevalid: "1px solid red" })
      : this.setState({ statevalid: "1px solid rgba(21, 21, 21, 0.15)" });
  }
  country1(e: any) {
    this.setState({ country: e.target.value });
    e.target.value === ""
      ? this.setState({ countryvalid: "1px solid red" })
      : this.setState({ countryvalid: "1px solid rgba(21, 21, 21, 0.15)" });
  }
  zipcode1(e: any) {
    this.setState({ zipcode: e.target.value });
    let zipstr = /^\d+$/.test(e.target.value.toString());
    e.target.value === "" || !zipstr
      ? this.setState({ zipcodevalid: "1px solid red" })
      : this.setState({ zipcodevalid: "1px solid rgba(21, 21, 21, 0.15)" });
  }
  phNo1(e: any) {
    this.setState({ phno: e.target.value });
    let phstr = /^\d+$/.test(e.target.value.toString());
    e.target.value === "" || !phstr
      ? this.setState({ phnovalid: "1px solid red" })
      : this.setState({ phnovalid: "1px solid rgba(21, 21, 21, 0.15)" });
  }

  handleSubmit(e: any) {
    e.preventDefault();
    let formvalid = true;
    if (this.state.state === "") {
      this.setState({ statevalid: "1px solid red" });
      formvalid = false;
    }
    if (this.state.country === "") {
      this.setState({ countryvalid: "1px solid red" });
      formvalid = false;
    }
    let zipstr = /^\d+$/.test(this.state.zipcode.toString());
    if (!zipstr) {
      this.setState({ zipcodevalid: "1px solid red" });
      formvalid = false;
    }
    let phstr = /^\d+$/.test(this.state.phno.toString());
    if (!phstr) {
      this.setState({ phnovalid: "1px solid red" });
      formvalid = false;
    }
    if (this.state.pwdChecked) {
      if (
        this.state.currentPwdCheck === "" ||
        this.state.currentPassword !== this.state.currentPwdCheck
      ) {
        this.setState({ pwdCheckBorder: "1px solid red" });
        formvalid = false;
      }
      if (this.state.newPwd === "") {
        this.setState({ pwdMatchBorder: "1px solid red" });
        formvalid = false;
      }
      if (this.state.confirmPwd === "") {
        this.setState({ pwdMatchBorder: "1px solid red" });
        formvalid = false;
      }
      if (this.state.newPwd !== this.state.confirmPwd) {
        formvalid = false;
      }
    }
    if (formvalid) {
      let finalpwd = this.state.confirmPwd;
      if (this.state.pwdChecked === false) {
        finalpwd = "";
      }
      let data = {
        state: this.state.state,
        country: this.state.country,
        zipcode: this.state.zipcode,
        phno: this.state.phno,
        password: finalpwd,
        logontype: this.state.logonType,
      };
      this.datasend(data);
    }
  }
  async datasend(data: any) {
    let res = await api.UpdateUserInfo(data);
    history.push("/");
  }

  componentDidMount() {
    this.onload();
  }
  async onload() {
    let res = await api.getUserInfo();
    console.log(res);
    this.setState({
      firstname: res.firstname,
      lastName: res.lastname,
      gender: res.gender,
      state: res.state,
      country: res.country,
      zipcode: res.zipcode,
      phno: res.phone_no1,
      email: res.email_add1,
      logonType: res.handle_login_type,
      currentPassword: res.user_password,
      loading: false,
    });
  }
  pwdHandleChange() {
    this.setState({ pwdChecked: !this.state.pwdChecked });
  }
  pwdChange(event: any) {
    this.setState({ currentPwdCheck: event.target.value });
    if (this.state.currentPassword !== event.target.value) {
      this.setState({
        passwordCheck: "block",
        pwdCheckBorder: "1px solid red",
      });
    } else {
      this.setState({ passwordCheck: "none", pwdCheckBorder: "" });
    }
  }
  newPwd(e: any) {
    this.setState({ newPwd: e.target.value });
    if (this.state.confirmPwd !== e.target.value) {
      this.setState({ newPwdMatch: "block", pwdMatchBorder: "1px solid red" });
    } else {
      this.setState({ newPwdMatch: "none", pwdMatchBorder: "" });
    }
  }
  confirmPwd(e: any) {
    this.setState({ confirmPwd: e.target.value });
    if (this.state.newPwd !== e.target.value) {
      this.setState({ newPwdMatch: "block", pwdMatchBorder: "1px solid red" });
    } else {
      this.setState({ newPwdMatch: "none", pwdMatchBorder: "" });
    }
  }

  render() {
    const pwdChange = this.state.pwdChecked ? (
      <>
        <div className="form-group row">
          <label htmlFor="currpwd" className="col-sm-3 col-form-label">
            Current Password
          </label>
          <div className="col-sm-9">
            <input
              type="password"
              className="form-control"
              id="currpwd"
              name="currentpwd"
              value={this.state.currentPwdCheck}
              onChange={(e) => this.pwdChange(e)}
              placeholder="Current Password"
              style={{ border: this.state.pwdCheckBorder }}
            />
          </div>
          <div
            className="emailexists col-sm-12"
            style={{ display: this.state.passwordCheck }}
          >
            *Current Password Does not Match
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="newpwd1" className="col-sm-3 col-form-label">
            New Password
          </label>
          <div className="col-sm-9">
            <input
              type="password"
              className="form-control"
              id="newpwd1"
              name="newpwd"
              value={this.state.newPwd}
              onChange={(e) => this.newPwd(e)}
              placeholder="New Password"
              style={{ border: this.state.pwdMatchBorder }}
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
              title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
              required
            />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="conpwd" className="col-sm-3 col-form-label">
            Confirm Password
          </label>
          <div className="col-sm-9">
            <input
              type="password"
              className="form-control"
              id="conpwd"
              name="confirmpwd"
              value={this.state.confirmPwd}
              onChange={(e) => this.confirmPwd(e)}
              placeholder="Confirm Password"
              style={{ border: this.state.pwdMatchBorder }}
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
              title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
              required
            />
          </div>
          <div
            className="emailexists col-sm-12"
            style={{ display: this.state.newPwdMatch }}
          >
            *New Password and Confirm Password Does not Match
          </div>
        </div>
      </>
    ) : null;
    return (
      <div>
        <div className="wrapper gray-wrapper">
          <div className="container inner">
            <h2 className="section-title text-center">Profile</h2>
            {this.state.loading ? <Loader/> : (
              <form
                onSubmit={(e) => this.handleSubmit(e)}
                className="profile-form mulifont"
              >
                <div className="form-group row">
                  <label htmlFor="fname" className="col-sm-3 col-form-label">
                    First Name
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="fname"
                      defaultValue={this.state.firstname}
                      placeholder="First Name"
                      readOnly={true}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="lname" className="col-sm-3 col-form-label">
                    Last Name
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="lname"
                      name="lastName"
                      value={this.state.lastName}
                      placeholder="Last Name"
                      readOnly={true}
                      // onChange={(e) => this.lastName(e)}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputEmail2"
                    className="col-sm-3 col-form-label"
                  >
                    Email
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="email"
                      className="form-control"
                      id="inputEmail2"
                      name="email"
                      defaultValue={this.state.email}
                      placeholder="Email"
                      readOnly={true}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="gender" className="col-sm-3 col-form-label">
                    Gender
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="gender"
                      defaultValue={this.state.gender}
                      placeholder="Gender"
                      readOnly={true}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="state" className="col-sm-3 col-form-label">
                    State
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="state"
                      name="state1"
                      value={this.state.state}
                      onChange={(e) => this.state1(e)}
                      placeholder="State"
                      style={{ border: this.state.statevalid }}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="country" className="col-sm-3 col-form-label">
                    Country
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="country"
                      name="country1"
                      value={this.state.country}
                      onChange={(e) => this.country1(e)}
                      placeholder="Country"
                      style={{ border: this.state.countryvalid }}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="zipcode" className="col-sm-3 col-form-label">
                    Zipcode
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control"
                      id="zipcode"
                      name="zipcode1"
                      value={this.state.zipcode}
                      onChange={(e) => this.zipcode1(e)}
                      placeholder="Zipcode"
                      style={{ border: this.state.zipcodevalid }}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="phnumber" className="col-sm-3 col-form-label">
                    Phone Number
                  </label>
                  <div className="col-sm-9">
                    <input
                      type="tel"
                      className="form-control"
                      id="phnumber"
                      name="phNo1"
                      value={this.state.phno}
                      onChange={(e) => this.phNo1(e)}
                      placeholder=""
                      style={{ border: this.state.phnovalid }}
                    />
                  </div>
                </div>
                {this.state.logonType === "ecanvas" ? (
                  <>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Change Password
                      </label>
                      <div className="col-sm-9">
                        <div className="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="changepwd"
                            checked={this.state.pwdChecked}
                            onChange={() => this.pwdHandleChange()}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="changepwd"
                          >
                            Yes
                          </label>
                        </div>
                      </div>
                    </div>
                    {pwdChange}
                  </>
                ) : null}

                <div className="form-group row">
                  <div className="col-sm-10">
                    <button type="submit" className="btn btn-full-rounded">
                      Save
                    </button>
                  </div>
                </div>
              </form>
            )}
          </div>
        </div>
      </div>
    );
  }
}
