import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import Popup from "reactjs-popup";
import { GoogleLogin } from "react-google-login";
import FacebookLogin from "react-facebook-login";
import { Globaldata } from "../container/sample";
import history from "../../history";
import { apiurlprefix } from "../../apiconfig";
import UserAvatar from "react-user-avatar";
import api from '../API/api';

interface IHeader {
  isLoggedIn: boolean;
  redirect: boolean;
  checkStatus: string;
  email: string;
  password: string;
  emailpwdCheck: string;
  typesearch: string;
  searchdata: string;
  btnLoader:boolean;
}

class Header extends Component<{}, IHeader> {
  constructor(props: {}) {
    super(props);
    this.state = {
      isLoggedIn: false,
      redirect: false,
      checkStatus: "",
      email: "",
      password: "",
      emailpwdCheck: "none",
      typesearch: "images",
      searchdata: "",
      btnLoader:false,
    };
  }

  signup() {
    this.renderRedirectReg();
  }
  

  renderRedirectReg() {
    $(".close").click();
    history.push("/reg");
  }
  renderRedirectHome() {
    history.push("/");
  }

  ecanvasSignup() {
    $(".close").click();
    history.push("/reg");
  }
  componentDidMount() {
    if (localStorage.getItem("logonid") !== null) {
      this.setState({ isLoggedIn: true });
    }
  }
  email(e: any) {
    this.setState({ email: e.target.value ,emailpwdCheck:'none'});
  }
  password(e: any) {
    this.setState({ password: e.target.value,emailpwdCheck:'none' });
  }
  searchChange(e: any) {
    this.setState({ searchdata: e.target.value });
  }
  category(e: any) {
    this.setState({ typesearch: e.target.value });
    if (this.state.typesearch == "images") {
      Globaldata.typesearch = 1;
    } else {
      Globaldata.typesearch = 0;
    }
  }
  clearData() {
    this.setState({ searchdata: "" });
  }

  searchSubmit(e: any) {
    e.preventDefault();
    if (this.state.searchdata != "") {
      Globaldata.searchData = this.state.searchdata;
      history.push(`/search/${Globaldata.searchData}/${Globaldata.typesearch}`);
      this.clearData();
    }
  }

  avatarRender(){
    let logontype = localStorage.getItem('logontype');
    let imgsrc;
    if(logontype == 'google'){
      imgsrc = localStorage.getItem('avatar');
      return(<UserAvatar size="48" name="user" src={imgsrc} />)
    }
    else{
      return( 
        <UserAvatar
        size="48"
        name='default' src='/assets/images/profile.png'
      />)
    }
  }

  async Logout(){
    this.setState({
      isLoggedIn: false,
    });
    let res = await api.Logout();
    this.renderLoginStatus();
    this.renderRedirectHome();
  };

  renderLoginStatus(){
    if (this.state.isLoggedIn) {
      return (
        <>

          <Nav.Item className="menu">
            <NavLink to="/uploadart" exact={true} className="text-white">
              Upload Art
            </NavLink>
          </Nav.Item>
          <Nav.Item className="menu">
            <NavLink to="/portfolio" exact={true} className="text-white">
              My Portfolio
            </NavLink>
          </Nav.Item>
          <Nav.Item className="menu">
            <NavLink to="/aboutus" exact={true} className="text-white">
              About Us
            </NavLink>
          </Nav.Item>
          <Nav.Item className="menu mobile_menu">
            <NavLink to="/myprofile" exact={true} className="text-white">
            My Profile
            </NavLink>
          </Nav.Item>
          <Nav.Item className="menu mobile_menu">
          <button className="btn avatar-popup-btn btn_logout" onClick={()=>this.Logout()}>Logout</button>
          </Nav.Item>
         <Nav.Item className="menu avatar-item">
                <Popup
                  trigger={
                    <button className="button avatar-btn">
                      {this.avatarRender()}
                    </button>
                  }
                  position="bottom center"
                  on="hover"
                >
                  <div>
                  <div className="form-group row">
                    <div className="col-sm-12 text-center">
                    <NavLink to="/myprofile" exact={true}>
                      <button className="btn avatar-popup-btn">My Profile</button>
                      </NavLink>
                      <button className="btn avatar-popup-btn" onClick={()=>this.Logout()}>Logout</button>
                    </div>
                    </div>
                  </div>
                </Popup>
              </Nav.Item>
        </>
      );
    } else {
      return (
        <>
          <Nav.Item className="menu">
            <NavLink to="/aboutus" exact={true} className="text-white">
              About Us
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <Popup
              trigger={
                <button
                  className="search_btn about login_button"
                  onClick={this.renderRedirectHome}
                >
                  Login
                </button>
              }
              modal
            >
              {(close) => (
                <div>
                  <button className="close" onClick={close}>
                    &times;
                  </button>
                  <div className="col-lg-8 col-sm-8 col-12 main-section">
                    <div className="modal-content">
                      <div className="col-lg-12 col-sm-12 col-12 user-name text-center">
                        <h2 className="section-title mb-40 ">Login</h2>
                      </div>
                      <div className="col-lg-12 col-sm-12 col-12 form-input">
                        <form onSubmit={(e)=>this.handleSubmit(e)}>
                          <div className="form-group">
                            <input
                              type="email"
                              className="form-control"
                              placeholder="Enter email"
                              value={this.state.email}
                              onChange={(e) => this.email(e)}
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="password"
                              className="form-control"
                              placeholder="Password"
                              value={this.state.password}
                              onChange={(e) => this.password(e)}
                            />
                          </div>
                          <div
                            className="checkemailpws"
                            style={{ display: this.state.emailpwdCheck }}
                          >
                            Enter Correct Email & Password
                          </div>
                          <button
                            type="submit"
                            className="btn btn-full-rounded"
                            disabled={this.state.btnLoader}
                          >
                            {this.state.btnLoader && (<i
                  className="fa fa-refresh fa-spin"
                  style={{ marginRight: "5px" }}
                />)}
                            Login
                          </button>
                        </form>
                        <button
                          className="btn btn-full-rounded"
                          onClick={this.ecanvasSignup}
                        >
                          Signup
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="or text-center">
                    <img src="/assets/images/or.png" />
                  </div>
                  <div className="montfont text-center login-popup">
                    <div className="socialBtn">
                      <GoogleLogin
                        clientId="108651366576-r11lcigdq0on3l3s7ih315lhldtrus65.apps.googleusercontent.com"
                        buttonText="Login With Google"
                        onSuccess={(response)=>this.responseGoogle(response)}
                        onFailure={(response)=>this.responseGoogle(response)}
                        cookiePolicy={"single_host_origin"}
                      />
                    </div>
                    <div className="socialBtn">
                      <FacebookLogin
                        appId="684572925702628"
                        autoLoad={false}
                        fields="name,email,picture"
                        callback={(response)=>this.responseFacebook(response)}
                        icon="fa-facebook"
                      />
                    </div>
                  </div>
                </div>
              )}
            </Popup>
          </Nav.Item>
        </>
      );
    }
  };

  async handleSubmit(e: any){
    e.preventDefault();
    this.setState({btnLoader:true});
    let res = await api.checkEcanvasUser(this.state.email,this.state.password);
    this.setState({checkStatus:res,btnLoader:false})
    if (this.state.checkStatus == "0") {
      this.setState({
        isLoggedIn: false,
        emailpwdCheck: "block",
      });
    } else {
      let result = await api.CurrentUser();
      this.setState({
        isLoggedIn: true,
        email: "",
        password: "",
      },()=>this.renderLoginStatus());
    }
    if(window.location.href.includes('search')){
      window.location.reload();
    }
  };
  async responseFacebook(response: any){
    localStorage.setItem("userMail", response.email);
    if (
      response.status != "unknown" &&
      localStorage.getItem("userMail") !== null
    ) {
      localStorage.setItem("userMail", response.email);
      localStorage.setItem("userName", response.name);
      localStorage.setItem("logontype", 'facebook');
      Globaldata.logintype = "facebook";
      let res = await api.checkSocialUser(response.email);
      this.setState({checkStatus:res})
      if (this.state.checkStatus == "0") {
        this.signup();
      } else {
        localStorage.removeItem("userMail");
        localStorage.removeItem("userName");
        let result = await api.CurrentUser();
        this.setState({
          isLoggedIn: true,
        },()=>this.renderLoginStatus());
      }
    }
    if(window.location.href.includes('search')){
      window.location.reload();
    }
  };

  async responseGoogle(response: any){
    localStorage.setItem("userMail", response.email);
    if (
      response.error != "popup_closed_by_user" &&
      localStorage.getItem("userMail") !== null
    ) {
      localStorage.setItem("userMail", response.profileObj.email);
      localStorage.setItem("userName", response.profileObj.givenName);
      localStorage.setItem("avatar", response.profileObj.imageUrl);
      localStorage.setItem("logontype", 'google');
      Globaldata.logintype = "google";
      let res = await api.checkSocialUser(response.profileObj.email);
      this.setState({checkStatus:res})
      if (this.state.checkStatus == "0") {
        this.signup();
      } else {
        localStorage.removeItem("userMail");
        localStorage.removeItem("userName");
        let result = await api.CurrentUser();
        this.setState({
          isLoggedIn: true,
        },()=>this.renderLoginStatus());}
    }
    if(window.location.href.includes('search')){
      window.location.reload();
    }
  };     
  
  render() {    
    return (
      <Navbar
        sticky="top"
        expand="lg"
        className="navbar solid nav-wrapper-dark navbar-expand-lg"
      >
        <div className="container">
          <Navbar.Brand>
            <NavLink to="/" exact={true}>
              <img
                src="/assets/images/ecanvasprologo.png.png"
                className="header_logo"
                alt="logo"
              />
            </NavLink>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="collapse navbar-collapse navbartext"
          >
            <Nav className="ml-auto">
            <div className='parent_search'>
              <div className='parent_child1'>
              <form onSubmit={(e) => this.searchSubmit(e)}>
                <input
                  type="text"
                  placeholder="Search.."
                  className="search"
                  onChange={(e) => this.searchChange(e)}
                  value={this.state.searchdata}
                />
              </form>
              </div>
              <div className="select-type custom-select-wrapper parent_child2">
                <select
                  className="custom-select"
                  value={this.state.typesearch}
                  onChange={(e) => this.category(e)}
                >
                  <option value="images">Images</option>
                </select>
              </div>
              <button
                className="search_btn"
                onClick={(e) => this.searchSubmit(e)}
              >
                Search
              </button>
              </div>
              {this.renderLoginStatus()}
            </Nav>
          </Navbar.Collapse>
          </div>
      </Navbar>
    );
  }
}

export default Header;
